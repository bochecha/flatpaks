GPG_KEYID := E8CFA3B3C849DA4567EC56182A41910290C72C72
REPO := ../repo-apps
BUILD_DIR := ./appdir
BUILD_CMD = flatpak-builder --ccache --force-clean --gpg-sign=$(GPG_KEYID) --repo=$(REPO) $(BUILD_DIR) $<


blender: org.blender.Blender.json
	$(BUILD_CMD)


picard: org.musicbrainz.Picard.json
	$(BUILD_CMD)
